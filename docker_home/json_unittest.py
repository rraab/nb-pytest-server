#!/usr/bin/env python3

"""
Run server-side unittests and build JSON output to return to students
"""

# assume paths are
# assignment.ipynb (location of notebook)
# test_assignment.py (location of unittests)

import re
import os
import sys
import json

import unittest as ut
from unittest.result import failfast

import test_assignment

debug = False
output = {}
total_points = [0]
points_awarded = {}


def fmt_testname(test):
    r = re.search("([\w]*) \(.*\.(.*)\)", str(test))
    return f"{r.group(2)}.{r.group(1)}"


class Result(ut.TestResult):
    """Patch TestResult class to record function output as points"""

    points = 0

    def addSuccess(self, test):
        self.points = test.points or 0
        total_points[0] += self.points
        points_awarded[fmt_testname(test)] = self.points

    @failfast
    def addFailure(self, test, err):
        errtype, value, traceback = err
        self.failures.append((test, value))


if not debug:
    # temporarily silence output streams
    _out = sys.stdout
    _err = sys.stderr
    sys.stdout = open(os.devnull, "w")
    sys.stderr = open(os.devnull, "w")

# define test suite and runner
suite = ut.defaultTestLoader.loadTestsFromModule(test_assignment)
runner = ut.TextTestRunner()
runner._makeResult = lambda: Result()

# run the tests
results = runner.run(suite)

if not debug:
    # restore output streams
    sys.stdout = _out
    sys.stderr = _err

# compile results
failures = {}
for test, value in results.failures:
    failures[fmt_testname(test)] = str(value)
for error in results.errors:
    # TODO handle errors
    pass

output["summary"] = {
    "tests run": results.testsRun,
    "successes": results.testsRun - len(results.errors) - len(results.failures),
    "failures": len(results.failures),
    "errors": len(results.errors),
    "tentative_score": total_points[0],
}

output["failures"] = failures
output["points_awarded"] = points_awarded

# print test results as json
print(json.dumps(output, indent=4))
