#!/usr/bin/env python3

'''
Python HTTP endpoints for student assignment submission script.

'/'          - check cruzid/password auth header and return assignment number
'/autograde' - save student submission and return test results
'''

import os
import pathlib
import json
import pytz
import datetime as dt
from subprocess import Popen, PIPE

import bottle as bot
bot.BaseRequest.MEMFILE_MAX = 1024 * 1024 * 10

TIMEZONE = pytz.timezone('America/Los_Angeles')

def get_auth_index(cruzid, password):
    '''
    get the 1-indexed assignment number for which (cruzid, password) is valid.
    return 0 if (cruzid, password) not valid
    '''
    with open("roster.json") as f:
        roster = json.load(f)

    return (cruzid in roster and roster[cruzid] == password)

@bot.post('/')
@bot.auth_basic(get_auth_index)
def index():
    '''
    Ensure that the HTTP auth headers are valid for
    this cruzid and password.
    Since passwords are unique to each student and assignment jointly,
    return the assignment number we infer from the password.
    '''
    return {"Authorized": "OK"}

@bot.post('/autograde')
@bot.auth_basic(get_auth_index)
def autograde():
    '''
    1. Extract cruzid and password from HTTP auth headers.
       Password are unique to each student and assignment jointly,
       so we can infer the assignment number.
    2. Save the submitted notebook JSON to file (ovewrite previous submissions).
    3. Run pytests against the notebook file corresponding to the assignment
       number.
    4. Log the pytest results to file (all pytest results are stored; no
       overwriting).
    '''

    now = dt.datetime.now(tz=TIMEZONE)
    cruzid = bot.request.auth[0]

    try:
        payload = bot.request.json
        assignment_number = payload['assignment_number']
        notebook_json = payload['notebook']
    except:
        result = {'Error': 'Server could not parse ipynb/json submission.'}
        return result

    # save submitted notebook json

    notebook_path = ensure_dir(
        f"notebooks_{assignment_number}/{cruzid}",
        f"{now.ctime().replace(' ', '_').replace(':', '-')}.ipynb"
    )

    save_json(notebook_json, notebook_path)

    # run autograding

    tests_path = ensure_dir(f"tests", f"test_{assignment_number}.py")

    result = run_autograder(notebook_path, tests_path)

    # log autograding results

    results_file_path = ensure_dir(
        f"results_{assignment_number}/{cruzid}",
        f"{now.ctime().replace(' ', '_').replace(':', '-')}.json"
    )

    save_json(result, results_file_path)

    # return results to student
    return result

def ensure_dir(dirname, filename):
    '''
    ensure directory exists and return the joint path
    '''
    pathlib.Path(dirname).mkdir(parents=True, exist_ok=True)
    return os.path.join(dirname, filename)

def save_json(payload, target_path):
    '''
    Save / overwrite last submission
    '''

    with open(target_path, 'w', encoding="utf-8") as f:
        json.dump(payload, f)

def run_autograder(notebook_path, tests_path):
    '''
    run autograding script
    '''

    if not os.path.exists(tests_path):
        return {
            'Error': f'{tests_path} does not exist on the server.'
        }

    if not os.path.exists(notebook_path):
        return {
            'Error': f'{notebook_path} was not written to server.'
        }

    p = Popen(
        f'python3 autograde.py {notebook_path} {tests_path}',
        shell=True, stdin=PIPE, stdout=PIPE, close_fds=True
    )
    stdout, _ = p.communicate()

    return json.loads(stdout.decode())

app = bot.default_app()
