#!/usr/bin/env python3

"""
Run pytest_json in an nsjail
with no network access and limited file access
"""

import sys
import json
import subprocess

NOTEBOOK_PATH = sys.argv[1]
TESTS_PATH = sys.argv[2]
TIMEOUT = 3  # seconds

# for debugging:
# nsjail --config nsjail.cfg -- $(which bash) -i

CMD = (
    "nsjail "
    f"-R /home/{NOTEBOOK_PATH}:/home/assignment.ipynb "
    f"-R /home/{TESTS_PATH}:/home/test_assignment.py "
    "-R /home/_ipynbimport.py:/home/_ipynbimport.py "
    "-R /home/pytest_json.py:/home/pytest_json.py "
    "--time_limit 30 "
    "--config nsjail.cfg "
    "-- $(which python3) json_unittest.py"
)

p = subprocess.Popen(
    CMD, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True
)

try:
    stdout, _ = p.communicate(timeout=TIMEOUT)
except subprocess.TimeoutExpired:
    print(json.dumps({"Error": f"Timeout expired running pytest: {TIMEOUT} seconds."}))
    exit()

result = json.loads(stdout.decode())

print(json.dumps(result))
