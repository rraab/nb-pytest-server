# syntax=docker/dockerfile:1

# A Docker image consists of read-only layers each of which represents a
# Dockerfile instruction. The layers are stacked, and each one is a delta of the
# changes from the previous layer.
#
# https://docs.docker.com/engine/reference/builder/

# base layer. Only one `FROM` statement per dockerfile
# https://github.com/google/nsjail
FROM nsjailcontainer

ENV TZ America/Los_Angeles
ENV PYTHON_VERSION 3.10.8

# needed to instll python dependencies
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#Set of all dependencies needed for pyenv to work on Ubuntu
RUN apt-get update &&\
    apt-get install -y --no-install-recommends \
        make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev \
        libsqlite3-dev wget ca-certificates curl llvm libncurses5-dev xz-utils \
        tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev \
        mecab-ipadic-utf8 git

# Set-up necessary vars for pyenv
ENV PYENV_ROOT /.pyenv
ENV PATH $PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH

# Install pyenv
RUN git clone https://github.com/pyenv/pyenv.git $PYENV_ROOT &&\
    pyenv install $PYTHON_VERSION &&\
    pyenv global $PYTHON_VERSION &&\
    pyenv rehash

# copy requirements
COPY requirements.txt ./

# install requirements
RUN pip3 install --upgrade pip && pip3 install -r requirements.txt

# If a Dockerfile has multiple CMDs, it only applies the instructions from the last one.
CMD ["python3", "serve.py"]
